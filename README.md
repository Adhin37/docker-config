# Version

| Name           | Version |
|----------------|---------|
| php            | 7.2.31  |
| docker-compose | 1.25.5  |
| docker         | 19.03.8 |
| lazydocker     | 0.9     |
| symfony        | 1.5.11  |
| composer       | 1.10.6  |

# Docker Install

``` bash
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

# Git

## Git submodule
git submodule update --init --recursive

## Git SSH
Setup an ssh key then ad it to bitbucket
https://support.atlassian.com/bitbucket-cloud/docs/set-up-an-ssh-key/

# Plugin Installation

## loki-docker-driver

You need to install the plugin on each Docker host with container from which you want to collect logs.

You can install the plugin from our Docker hub repository by running on the Docker host the following command:

``` bash
docker plugin install  grafana/loki-docker-driver:master-4ce385f --alias loki --grant-all-permissions
```

To check the status of installed plugins, use the docker plugin ls command. Plugins that start successfully are listed as enabled in the output:

``` bash
docker plugin ls
ID                  NAME         DESCRIPTION           ENABLED
ac720b8fcfdb        loki         Loki Logging Driver   true
```

restart docker

``` bash
sudo systemctl restart docker
```

## Install logcli to debug loki

export loki url

``` bash
export LOKI_ADDR=http://localhost:3100
```

## Install mongo php extension

``` bash
sudo add-apt-repository ppa:ondrej/php -y
sudo apt-get update
sudo apt-get install php-pear php7.2-dev libcurl3-openssl-dev apache2 libapache2-mod-php7.2
sudo pecl install mongodb
```

Finally, add the following line to your php.ini file:

``` bash
vim /etc/php/7.2/apache2/php.ini
extension=mongodb.so
```

## lazydocker

To monitor all docker process globally use lazydocker

``` bash
sudo curl https://raw.githubusercontent.com/jesseduffield/lazydocker/master/scripts/install_update_linux.sh | bash
```
